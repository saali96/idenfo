import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/signup',
        name: 'signup',
        component: () => import('../components/Signup.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../components/Login.vue')
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('../components/Home.vue')
    },
    {
        path: '/checkout',
        name: 'checkout',
        component: () => import('../components/CheckOut.vue')
    },
    {
        path: '/checkin',
        name: 'checkin',
        component: () => import('../components/CheckIn.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router