import axios from 'axios'
import router from "../router"

const client = axios.create({
    baseURL: 'http://localhost:8080/',
    json: true
})

export default {
    async execute(method, resource, data) {
        // inject the accessToken for each request
        let accessToken = localStorage.getItem('token')

        return client({
            method,
            url: resource,
            data,
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }).then(req => {
            return req.data
        })
    },
    async login(body) {
        let data = {
            email: body.email,
            password: body.password
        }
        axios.post("http://localhost:8080/librarian/login/", data)
            .then(response => {
                if (response) {
                    console.log(response.data.message)
                    localStorage.setItem('token', response.data.data.payload.token)
                    router.push("/home")
                }
            })
            .catch(errors => {
                console.log(errors.response.data.message)
            })
    },
    signup(body) {
        let data = {
            email: body.email,
            password: body.password
        }
        axios.post("http://localhost:8080/librarian/signup/", data)
            .then(response => {
                if (response) {
                    console.log(response.data.message)

                    router.push("/login")
                }
            })
            .catch(errors => {
                console.log(errors.response.data.message)
            })
    },
    getAllBooks() {
        return this.execute('get', '/book/list')
    },
    getBook(id) {
        console.log(id)
        return this.execute('get', `/book/find/?id=` + id)
    },
    checkOut(data) {
        return this.execute('put', '/book/checkout', data)
    },
    checkIn() {
        var id = { id: localStorage.getItem('selectedBook') }
        console.log(id)
        return this.execute('put', '/book/checkin', id)
    },
}
